#!/bin/sh

zcat $1 | sed 's/\sDEFINER=`[^`]*`@`[^`]*`//g' | docker exec -i $2 sh -c 'mysql -u root -p"$MYSQL_ROOT_PASSWORD" $MYSQL_DATABASE'
