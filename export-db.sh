#!/bin/bash
NOW=$(date +"%m-%d-%Y_%H:%M")
docker-compose exec db sh -c 'exec mysqldump -uroot -p"$MYSQL_ROOT_PASSWORD" "$MYSQL_DATABASE"' | gzip > db_backups/$NOW.sql.gz
