#!/bin/bash
# accepts base url as argument, must be in format like http://verticalxpress.localhost/

IS_URL_REGEX='(https?)://[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]'

#if no argument passed, prompt for url.
BASEURL=""
while [ -z $BASEURL ]; do
  if [ $# -eq 0 ]; then
    echo "Enter base url: "
    read BASEURL;
  else
    BASEURL=${1}
  fi

  if [[ $BASEURL =~ $IS_URL_REGEX ]]; then
    echo "isvalid"
    break;
  else
    echo "Must enter a valid url."
    BASEURL=""
  fi
done

docker-compose exec apache bash -c "bin/magento config:set web/unsecure/base_url $BASEURL \
&& bin/magento config:set web/secure/base_url $BASEURL"
